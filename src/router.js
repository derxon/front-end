import { createRouter, createWebHistory } from "vue-router";

import App from './App.vue';

import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'
import Account from './components/Account.vue'
import Vehiculos from './components/Vehiculos.vue'
import VehiculoCreate from './components/VehiculoCreate.vue'
import Portafolio from './components/Portafolio.vue'
import PortafolioCreate from './components/PortafolioCreate.vue'
import Ruta from './components/Ruta.vue'
import RutaCreate from './components/RutaCreate.vue'
import Reservacion from './components/Reservacion.vue'
import ReservacionCreate from './components/ReservacionCreate.vue'


const routes = [{
        path: '/',
        name: 'root',
        component: App
    },
    {
        path: '/user/logIn',
        name: "logIn",
        component: LogIn
    },
    {
        path: '/user/signUp',
        name: "signUp",
        component: SignUp
    },
    {
        path: '/user/home',
        name: "home",
        component: Home
    },
    {
        path: '/user/account',
        name: "account",
        component: Account
    },
    {
        path: '/user/vehiculos',
        name: "vehiculos",
        component: Vehiculos
    },
    
    {
        path: '/user/vehiculoCreate',
        name: "vehiculoCreate",
        component: VehiculoCreate
    },
    {
        path: '/user/portafolio',
        name: "portafolio",
        component: Portafolio
    },
    {
        path: '/user/portafolioCreate',
        name: "portafolioCreate",
        component: PortafolioCreate
    },
    {
        path: '/user/ruta',
        name: "ruta",
        component: Ruta
    },
    {
        path: '/user/rutaCreate',
        name: "rutaCreate",
        component: RutaCreate
    },
    {
        path: '/user/reservacion',
        name: "reservacion",
        component: Reservacion
    },
    {
        path: '/user/reservacionCreate',
        name: "reservacionCreate",
        component: ReservacionCreate
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;